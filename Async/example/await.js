
// async mean wait for data until retrived.
// wait for code to execute.
const fetchUsers = async function (url) {
    const res = await fetch(url);
    const data = await res.json();
    console.log(data);
}

const d = fetchUsers("https://jsonplaceholder.typicode.com/users");