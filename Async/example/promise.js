/* const posts = [
  { title: "Post 1", content: "This is the content of post 1" },
  { title: "Post 2", content: "This is the content of post 2" },
];


const getPosts = function() {
    setTimeout(funtion() {
        let output = "";
        posts.forEach((post, index) => {
            output += `<li>${post.title}</li>`
        }
        document.body.innerHTML = output;
    }, 1000);
};


const createPost = (post)=>{
    return new Promise((resolved, rejects)=>{
        setTimeout(()=> {
            posts.push(post);
            const done = true;
            if (done){
                resolved();
            } else {
                rejects('Error .....');
            }
        }, 2000);
    });
};



createPost({title:'post 3',content:'This is content of post 3'}).then(getPosts).catch((err)=> {
    console.log(err);
}); */



// Promise all 
/* const p1 = new Promise( (resolved, reject)=>{
    setTimeout(resolved, 2000, 'Hi');
}); */

const p1 = Promise.resolve("Hi");
const p2 = Promise.resolve("Hello");
const p3 = fetch("https://jsonplaceholder.typicode.com/users").then((res) => 
    res.json()
);

Promise.all([p1, p2,  p3]).then((vals)=> {
    console.log(vals);
});