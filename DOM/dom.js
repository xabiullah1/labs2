/* const heading = document.getElementById('head');
console.log(heading.textContent); */

/* const list = document.getElementById('list')
console.log(list.innerHTML); */

/* const list = document.querySelector(".listc")
console.log(list); */

/* const list2 = document.querySelectorAll('.head')
console.log(list2);
 */

/* 
const list2 = document.getElementById('head')
list2.className += ' changeBG';
console.log(list2);
console.log(list2.className);
list2.classList.remove('changeBG')
console.log(list2.className);
list2.classList.add("changeBG");
 */



// const body = document.getElementById('body')
// console.log(body.innerHTML);
/* body.classList.add('changeBgColor')


const paraText = document.getElementById('division')
// console.log(paraText.innerHTML);
paraText.classList.add('changeBG')
paraText.innerText = "This is new"




const list = document.querySelectorAll('ul li')
console.log(list);
for (var i=0; i < list.length; i++){
    list[i].style.backgroundColor = 'blue';
}

list[0].style.cssText='background-color:green'
 */


// DOM Actions
// Selecting a class with name head
/* const heading = document.querySelector('.head');
const btn = document.querySelector('.btn');
 */
/* btn.onclick = () => {
    console.log('You have clicked the button');
}
btn.onmouseout = () => {
    heading.style.backgroundColor = 'green';
}
btn.onmouseover = () => {
    (heading.style.backgroundColor = "red");
} */

// Another way of action on click
/* function clickMe(){
    console.log('Clicked');
} */

// Add Event Listener
/* btn.addEventListener('click',e=>{
    heading.style.cssText = 'background-color: gold;color: red';
}) */


/* const paragraph = document.querySelector('.para');
console.log(paragraph.getAttribute('id'));

paragraph.setAttribute("style", "background-color:red")
console.log(paragraph.hasAttribute("style")); */


// Navigation throgh DOM elements
/* const listItems = document.getElementById("list-item");
const list = document.querySelector(".list");

console.log(listItems.parentNode);
console.log(listItems.parentElement);
console.log(listItems.parentNode.parentNode);
console.log(list.childNodes);
console.log(list.children);
console.log(list.firstElementChild);

console.log(listItems.previousElementSibling);
console.log(listItems.nextElementSibling); */

// Creating elements using DOM
const newEl = document.createElement("li");
/* console.log(newEl); */

const textData = document.createTextNode("News");
console.log(textData);

newEl.appendChild(textData);
console.log(newEl);

const list = document.querySelector('.list');
list.appendChild(newEl);

list.insertBefore(newEl, list.children[2]);


list.removeChild(newEl);