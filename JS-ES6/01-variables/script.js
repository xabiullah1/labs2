/* const kid = 'Wessam';
const gender = 'Female';

if (gender === 'male'){
    console.log(kid + ' This is my son');
}else {
    console.log(kid + ' This is my daughter');
}



const prof = 'dev'
if (prof === 'composer') {
    console.log(prof + ' is music creator');
} else if(prof === 'dev') {
    console.log(prof + ' is a developer');
} else {
    console.log(prof + ' Do not match');
}

console.log(1 /0 );
console.log('no number' / 2.5);


const firstName = 'Ismail';
const secondName = 'Mohammed';

console.log(`This is my name ${firstName} ${secondName}`);
 */

// We are going to explain comparision with boolean
/* 
let nameIsChecked = true;
let ageIsChecked = false;

let isGreater = 4>1;
console.log(isGreater); */


/* let id = Symbol();
Symbol('Ismail') === Symbol('Ismail') */


/* let a = 5;
console.log(a);
let b = 10;
console.log(b);
let c = a;
a = b;
console.log(a);
b = c;
console.log(c); */

/* let a = 5;
a++; //a=a+1
a+=5; //a=a+5

console.log(a); */

// Conditional Statement
/* let age = 20;
if (age == '20'){
    console.log('yes');
}else if( age > 10) {
    console.log('Age is greater than 10');
}else {
    console.log('no');
} */


/* let age = 14;
switch(age){
    case 12 :
        console.log('This is 12');
        break;
    case 14 :
        console.log('This is 14');
        
    case 15 :
        console.log('This is 15');
        break;
    default:
        console.log('NO');
        break;
} */


/* let age = 20;
let result = age > 20 ? true : false;
console.log(result); */

/* let age = 20;
let name = 'Ismail';
let salary = '1000';

let person = {
    name: 'Ismail',
    age: '20',
    salary: '1000'
};
console.log(person.name); // access property
console.log(person['age']) // access by index / key

delete person.salary;
console.log(person); */

/* let ages = [12, 13, 40, 50];
console.log(ages[0]);
console.log(ages.length);
console.log(ages[0] = 1);
ages.pop()
console.log(ages);
ages.shift();
console.log(ages);
ages.unshift(60);
console.log(ages);

ages.push(100);
console.log(ages);
ages.splice(3,1)
console.log(ages);

ages.forEach((element)=>{
    console.log(element);
})

console.log(ages.indexOf(40)); */


// Priting names of array

/* let names = ['ismail', 'zabi', 'mohammed']
console.log(names);
console.log(names.join('-'));

let arr1 = [1,2,3,4]
let arr2 = [...arr1,5]
console.log(arr2);
*/

// Copy data between objects

/* let obj1 = {
    greetings: 'Hello',
    name: 'Ismail',
    age: 23
};

let obj2 = {
    ...obj1,
    greetings: 'Hi'
};

console.log(obj2); */

// While loop

/* let i = 1;
let n = 5;
let arr = [];
while(i<=n){
    console.log(`The number is ${i}`);
    if(i>2){
        arr.push(i);
    };
    i++;
};
console.log(arr); */


// for loop

/* for(let row=0; row <=7; row++){
    console.log('any');
} */

// Using for in

/* const students = {
    name: 'Ismail',
    class: 1,
    age: 3
};

for(let key in students){
    console.log(`${key} =====> ${students[key]}`);
}
 */

// Using for of
/* const students = ['Ismail', 'Mohammed', 'Zabi'];
for(let el of students){
    console.log(el);

} */




// Functions

/* function passExam(name,score){
    const passUniversity = 71;
    const passCollege = 51;

    if(score >= passUniversity){
        console.log(name + ' Enrolled with University with ' + score + ' Points');
    } else if (score >= passCollege && score < passUniversity){
        console.log(name + " Enrolled with college with " + score + " Points");
    }else{
        console.log(`${name}, you have failed`);
    }
}

// passExam('Ismail',12)

function totalScore(practical, theoratical){
    return practical + theoratical;
}

passExam('Ismail', totalScore(31,40)) */


// arrow function
const multi = () => console.log(10*5);
console.log(multi());

/* let add = function(x,y){
    return x+y
} */

let add = (x,y) => x+y;

console.log(add(10,30));
