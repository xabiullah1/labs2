/* app.get('/api/courses/:id', function (req, res) {
    res.send(req.params.id);
    console.log(res);
    
})

app.get("/", (req, res)=> {
    res.send("Welcome to Real World Implementation in Node JS");
})

const port = process.env.PORT | 4000
app.listen(port, function () {
    console.log(`You are Listening to Port ${port}`);
})

// Handling Port Dynamic from process
 */
const { response, application, json } = require("express");
const express = require("express");

const app = express();

app.use(express.json());
const Joi = require("joi");

if (process.env.ENV_VAR !== 'production') {
    const morgan = require("morgan");
    app.use(morgan("combined"));
}

const config = require("config");
switch (process.env.ENV_VAR) {
  case "production":
    console.log(config.get("student"));
    break;
  case "development":
    console.log(config.get('student1'));
    break;
}
// const student1 = config.get("student1");
// const student2 = config.get("student2");
// console.log(student);
// console.log(student1);
// console.log(student2);





app.use(express.urlencoded());
const products = [
  { id: 1, name: "product 1", price: 10 },
  { id: 2, name: "product 2", price: 102 },
  { id: 3, name: "product 3", price: 103 },
  { id: 4, name: "product 4", price: 104 },
  { id: 5, name: "product 5", price: 105 },
  { id: 6, name: "product 6", price: 106 },
  { id: 7, name: "product 7", price: 107 },
  { id: 8, name: "product 8", price: 180 },
  { id: 9, name: "product 9", price: 109 },
  { id: 10, name: "product 10", price: 1010 },
];

app.get("/api/products", function (request, response) {
  response.send(products);
});

const port = process.env.PORT | 4000;
app.listen(port, function () {
  console.log(`You are Listening to Port ${port}`);
});

// get product from id
/* app.get('/api/products/:id', (request, response) => {
    const product = products.find((p) => p.id === parseInt(request.params.id));
    // Check if product not found
    if (!product) {
        response.status(404).send('the product not found');
    } 
    response.send(product);
}); */

// post method
/* app.post("/api/products", (request, response) => {
    data validation
    if (!request.body.name || request.body.length < 3) {
        response.status(404).send("name is required and the length should be minimum of 3")
    }
    push Object to an array
    const product = {
        id: products.length + 1,
        name: request.body.name,
        price: request.body.price

    };
    products.push(products);
    response.send(product)
});
 */

// Put Method
app.put("/api/products/:id", (request, response) => {
  //search if item exists
  const product = products.find((p) => p.id === parseInt(request.params.id));
  if (!product) {
    response.status(404).send("Not Found");
  }
  product.price = request.body.price;
  response.send(product);
  
});

app.delete("/api/products/:id", (request, response) => {
  // seach for id
  const product = products.find((p) => p.id === parseInt(request.params.id));
  if (!product) {
    response.status(404).send("Not Found");
  }
  const index = products.indexOf(product);
  products.splice(index, 1);
  console.log(index);
  response.send(products);
});

app.post("/api/products", (request, response) => {
  // define schema
  const schema = Joi.object({
    id: Joi.number(),
    name: Joi.string().alphanum().min(3).max(30).required(),
    price: Joi.number().required(),
  });

  const product = {
    id: products.length + 1,
    name: request.body.name,
    price: request.body.price,
  };
  // validate schema against object
  const { error } = schema.validate(product);
  if (error) response.status(404).send(error);
  products.push(products);
  response.send(product);
});


// Using static middleware to asses static files
// With this, localhost:4000/readme.txt can be accessed
app.use(express.static("assets"));

// Using midleware for URLENCODE
// ?name=food&price=3000

console.log(process.env.ENV_VAR);



