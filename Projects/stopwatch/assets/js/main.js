window.onload = function() {
    var seconds = 00;
    var minutes = 00;
    var appendMinutes = document.getElementById("minutes");
    var appendSeconds = document.getElementById("seconds");
    var buttonStart = document.getElementById("btn-start");
    var buttonStop = document.getElementById("btn-stop");
    var buttonReset = document.getElementById("btn-reset");
    var interval;

    buttonStart.onclick = function(){
        interval = setInterval(startTime, 10);
    }
    buttonStop.onclick = function() {
            clearInterval(interval);
    }
    buttonReset.onclick = function() {
            appendSeconds.innerHTML = "00";
            appendMinutes.innerHTML = "00";
    }

    function startTime(){
        minutes++;
        if(minutes <= 9){
            appendMinutes.innerHTML = "0" + minutes;
        }
        if(minutes > 9) {
            appendMinutes.innerHTML = minutes;
        }
        if (minutes>99) {
            seconds++;
            appendSeconds.innerHTML = "0" + seconds;
            minutes = 0;
            appendMinutes.innerHTML = "0" + 0;
        }
        if (seconds > 9) {
            appendSeconds.innerHTML = seconds;
        }
    }

}