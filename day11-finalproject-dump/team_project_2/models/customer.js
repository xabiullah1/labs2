const Joi = require("joi");
const mongoose = require("mongoose");

const Customer = mongoose.model(
  "Customer",
  new mongoose.Schema({
    name: {
      type: String,
      required: true,
      minlength: 3,
      maxlength: 255,
    },
    mobile: {
      type: String,
      required: true,
      minlength: 3,
      maxlength: 100,
    },
    isGold: {
      type: Boolean,
      default: false,
    },
    age: {
      type: Number,
      required: true,
    },
  })
);

function validateCustomer(customer) {
  const pattern = "^[+]?[(]?[0-9]{3}[)]?[-s.]?[0-9]{3}[-s.]?[0-9]{4,6}$";
  customer.age = parseInt(customer.age);
  if (customer.isGold) {
    customer.isGold = true;
  } else {
    customer.isGold = false;
  }
  const schema = Joi.object({
    name: Joi.string().min(3).max(255).required(),
    mobile: Joi.string().length(10).pattern(new RegExp(pattern)).required(),
    isGold: Joi.boolean(),
    age: Joi.number().required(),
  });
  return schema.validate(customer);
}

exports.Customer = Customer;
exports.validate = validateCustomer;
