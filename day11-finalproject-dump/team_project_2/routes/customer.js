const express = require("express");
const router = express.Router();
const { validate, Customer } = require("../models/customer");

router.get("/", async (req, res) => {
  const customers = await Customer.find().sort("name");
  res.render("index", { customers: customers });
});
router.get("/name", async (req, res) => {
  const customers = await Customer.find().sort("name");
  res.render("index", { customers: customers });
});
router.get("/age", async (req, res) => {
  const customers = await Customer.find().sort("age");
  res.render("index", { customers: customers });
});
router.get("/mobile", async (req, res) => {
  const customers = await Customer.find().sort("mobile");
  res.render("index", { customers: customers });
});
router.get("/isGold", async (req, res) => {
  const customers = await Customer.find().sort("isGold");
  res.render("index", { customers: customers });
});
router.get("/add", (req, res) => {
  res.render("add-customer", { errorInput: undefined });
});
router.get("/show/:id", async (req, res) => {
  const customer = await Customer.findById(req.params.id);
  if (!customer)
    return res
      .status(404)
      .send("The Customer with the given ID was not found.");
  res.render("show-customer", {
    customer: customer,
    errorInput: "",
  });
});

router.get("/delete/:id", async (req, res) => {
  let customer = await Customer.findByIdAndRemove(req.params.id);
  if (!customer)
    return res
      .status(404)
      .send("The Customer with the given ID was not found.");
  res.redirect("/api/customer");
});
router.put("/customers/:id", async (req, res) => {
  let customer1 = await Customer.findById(req.params.id);
  const { error } = validate(req.body);
  //validate input
  if (error)
    return res.status(400).render("show-customer", {
      customer: customer1,
      errorInput: error.details[0].message,
    });
  //.render("/customers/:id", { errorInput: error.details[0].message });

  console.log(req.body);
  let customer2 = await Customer.findByIdAndUpdate(req.params.id, {
    name: req.body.name,
    mobile: req.body.mobile,
    isGold: req.body.isGold,
    age: req.body.age,
  });
  //if (!customer) return res.status(404).send("The Customer with the given ID was not found.");
  if (!customer2)
    return res.status(404).render("show-customer", {
      customer: customer1,
      errorInput: "The Customer with the given ID was not found.",
    });
  res.redirect("/api/customer");
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  //schema.validate({ a: "a string" });
  //if (error) return res.status(400).send(error.details[0].message);
  if (error)
    return res
      .status(400)
      .render("add-customer", { errorInput: error.details[0].message });

  let customer = new Customer({
    name: req.body.name,
    mobile: req.body.mobile,
    isGold: req.body.isGold,
    age: req.body.age,
  });
  customer = await customer.save();
  res.redirect("/api/customer");
});

router.delete("/customer/:id", async (req, res) => {
  let customer1 = await Customer.findById(req.params.id);
  const { error } = validate(req.body);
  //validate input
  if (error)
    return res.status(400).render("show-customer", {
      customer: customer1,
      errorInput: error.details[0].message,
    });
  //.render("/customers/:id", { errorInput: error.details[0].message });

  console.log(req.body);
  let customer2 = await Customer.findByIdAndDelete(req.params.id);
  //if (!customer) return res.status(404).send("The Customer with the given ID was not found.");
  if (!customer2)
    return res.status(404).render("show-customer", {
      customer: customer1,
      errorInput: "The Customer with the given ID was not found.",
    });
  res.redirect("/api/customer");
});

module.exports = router;
