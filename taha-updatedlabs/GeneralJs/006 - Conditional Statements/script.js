// const kid = 'Alexis';
// const gender = 'female';

// if (gender === 'male') {
// 	console.log(kid + ' is my son');
// } else {
// 	console.log(kid + ' is my daughter');
// }

// const prof = 'developer';

// if (prof === 'composer') {
// 	console.log(prof + ' teaches students');
// } else if (prof === 'composer') {
// 	console.log(prof + ' creates music');
// } else {
// 	console.log('Professions do not match');
// }

// && || !

if (!5 === 5) {
	console.log('Condition is true');
} else {
	console.log('Condition is false');
}
