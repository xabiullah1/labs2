// const arr = ['Ann', 'Bob', 'John', 10, true, [1, 2, 3]];
// console.log(arr);
// console.log(arr[5][1]);

const colors = ['white', 'black', 'red'];
colors[1] = 'green';
colors.push('blue');
colors.pop();
colors.shift();
colors.unshift('purple');
console.log(colors.indexOf('yellow'));

console.log(colors);
