// to install mongoose npm i mongoose
//1- add mangoos package
// const {default: mongoose} = require("mongoose");
const mongoose = require("mongoose");

// 2- connect to database
mongoose
  .connect("mongodb://localhost/anydatabasename")
  .then(() => console.log(`connection to database done `))
  .catch((err) => console.log(`you have issue =======> ${err}`));

// 3 remove database warning

// mangoose
//   .connect("mongodb://localhost/NITA")
//   .then(() => console.log(`connection to database done `))
//   .catch((err) => console.log(`you have issue =======> ${err}`));

// Models are fancy constructors compiled from Schema definitions. An instance of a model is called a document. Models are responsible for creating and reading documents from the underlying MongoDB database.
//4- collection(root) = table --- document = row
// schema type
/* 
  MongoDB supports many datatypes. Some of them are −

String − This is the most commonly used datatype to store the data. String in MongoDB must be UTF-8 valid.

Integer − This type is used to store a numerical value. Integer can be 32 bit or 64 bit depending upon your server.

Boolean − This type is used to store a boolean (true/ false) value.

Double − This type is used to store floating point values.

Min/ Max keys − This type is used to compare a value against the lowest and highest.

Arrays − This type is used to store arrays or list or multiple values into one key.

Timestamp − ctimestamp. This can be handy for recording when a document has been modified or added.

Object − This datatype is used for embedded documents.

Null − This type is used to store a Null value.

Symbol − This datatype is used identically to a string; however, it's generally reserved for languages that use a specific symbol type.

Date − This datatype is used to store the current date or time in UNIX time format. You can specify your own date time by creating object of Date and passing day, month, year into it.

Object ID − This datatype is used to store the document’s ID.

Binary data − This datatype is used to store binary data.

Code − This datatype is used to store JavaScript code into the document.

Regular expression − This datatype is used to store regular expression. */

// 2- create schema
//
// Models are fancy constructors compiled from Schema definitions. An instance of a model is called a document.
//  Models are responsible for creating and reading documents from the underlying MongoDB database.
const studentSchema = new mongoose.Schema({
  name: String,
  age: Number,
  RegDate: {type: Date, default: Date.now},
  isActive: Boolean,
  phones: [String],
});

// 3- convert schema to model (class ) you want to insert this data (get / set )
const Student = mongoose.model("Studentdat", studentSchema);

async function createStudentDocument() {
  // take object from student and insert default value
  const student = new Student({
    name: "Zabi Mohammed",
    age: 15,
    isActive: true,
    phones: "999999",
  });
  // create
  const result = await student.save();
  console.log(result);
}

// createStudentDocument();

// ObjectId to check and validate if both users using the same row in the same time change something to prevent issue
//MongoDB uses ObjectIds as the default value of _id field of each document,
//which is generated while the creation of any document.
//The complex combination of ObjectId makes all the _id fields unique.

// first fill your database of randome data
/**
 *  @description Query document
 *
 */

async function getStudent() {
  const students = await Student.find({
    // age: 15,
    isActive: true, //or
    phones: "999999",
  })
    .limit(3) // .limit to retrive how many documents
    .sort({name: 1}) // sort by name ascending = 1 , descending = -1
    .select({name: 1, age: 1}); // select only name and age
  console.log(students);
};
// getStudent();

/**
 *  @description Comparison Query Operators
 *
 */

/* $eq
Matches values that are equal to a specified value.
$gt
Matches values that are greater than a specified value.
$gte
Matches values that are greater than or equal to a specified value.
$in
Matches any of the values specified in an array.
$lt
Matches values that are less than a specified value.
$lte
Matches values that are less than or equal to a specified value.
$ne
Matches all values that are not equal to a specified value.
$nin
Matches none of the values specified in an array. */

async function getStudent() {
  const students = await Student
    // .find({
    //   age: {$gte: 14, $lte: 30},
    // })
    .find({age: {$in: [2, 15, 20, 3, 4]}}) // Matches any of the values specified in an array.
    .or({isActive: true})//, {name: "Ismail Mohammed"})
    .sort({name: 1}); // sort by name ascending = 1 , descending = -1
  console.log(students);
}
// getStudent();

/**
 *  @description Regular expression
 *
 */
// Provides regular expression capabilities for pattern matching strings in queries

/* 

Period, matches a single character of any single character, except the end of a line. For example, the below regex matches shirt, short and any character between sh and rt.

1
sh.rt
^ Carat, matches a term if the term appears at the beginning of a paragraph or a line. For example, the below regex matches a paragraph or a line starts with Apple.

1
^Apple
^ Carat inside a bracket, for example, the below regex matches any characters but a, b, c, d, e.

1
[^a-e]
$ Dollar sign, matches a term if the term appears at the end of a paragraph or a line. For example, the below regex matches a paragraph or a line ends with bye.

1
bye$
[ ] Square brackets, matches any single character from within the bracketed list. For example, the below regex matches bad, bed, bcd, brd, and bod.

1
b[aecro]d
– Hyphen, used for representing a range of letters or numbers,often used inside a square bracket. For example, the below regex matches kam, kbm, kcm, k2m, k3m, k4m and k5m.

1
k[a-c2-5]m
( ) Parentheses, groups one or more regular expressions. For example, the below regex matches codexpedia.com, codexpedia.net, and codexpedia.org.

1
codexpedia\.(com|net|org)
{n} Curly brackets with 1 number inside it, matches exactly n times of the preceding character. For example, the below regular expression matches 4 digits string, and only four digits string because there is ^ at the beginninga nd $ at the end of the regex.

1
^[\d]{4}$
{n,m} Curly brackets with 2 numbers inside it, matches minimum and maximum number of times of the preceding character. For example, the below regular expression matches google, gooogle and goooogle.

1
go{2,4}gle
{n,}, Curly brackets with a number and a comma, matches minimum number of times the preceding character. For example, the below regex matches google, gooogle, gooooogle, goooooogle, ….

1
go{2,}gle
| Pipe, matches either the regular expression preceding it or the regular expression following it. For example, the below regex matches the date format of MM/DD/YYYY, MM.DD.YYYY and MM-DD-YYY. It also matches MM.DD-YYYY, etc.

1
^(0[1-9]|1[012])[-/.](0[1-9]|[12][0-9]|3[01])[-/.][0-9]{4}$
? Question mark, matches 1 or 0 character in front of the question mark. For example, the below regular expression matches apple and apples.

1
apples?
* Asterisk, matches 0 or more characters in front of the asterisk. For example, the below regular expression matches cl,col,cool,cool,…,coooooooooool,…

1
co*l
+ Plus, matches 1 or more characters in fron of the plus. For example, the below regular expression matches col,cool,…,cooooooooooool,…

1
co+l
! Exclamation, do not matches the next character or regular expression. For example, the below regular expression matches the the characher q if the charachter after q is not a digit, it will matches the q in those strings of abdqk, quit, qeig, but not q2kd, sdkq8d.

1
q(?![0-9])
\ Backslash, turns off the special meaning of the next character. For example, the below regex treats the period as a normal character and it matches a.b only.

1
a\.b
\b Backslash and b, matches a word boundary. For example, “\bwater” finds “watergun” but not “cleanwater” whereas “water\b” finds “cleanwater” but not “watergun”.
\n Backslash and n, represents a line break.
\t Backslash and t, represents a tab.
\w Backslash and w, it is equivalent to [a-zA-Z0-9_], matches alphanumeric character or underscore. Conversely, Capital \W will match non-alphnumeric character and not underscore.
\d Backslash and d, matches digits 0 to 9, equivalent to [0-9] or [:digit]
[:alpha:] or [A-Za-z] represents an alphabetic character.
[:digit:] or [0-9] or [\d] represents a digit.
[:alnum:] or [A-Za-z0-9] represents an alphanumeric character.

This regex matches email addresses

1
\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b
This regex matches websites links ending with sites of .com, .org, .edu, .gov and .us

1
https?://(www\.)?[A-Za-z0-9]+\.(com|org|edu|gov|us)/?.*
This regex matches social security numbers.

1
^[0-9]{3}-[0-9]{2}-[0-9]{4}$

*/

// ^ : Caret // name start with mariam

async function getStudent() {
  const students = await Student
    // .find({name:/^mariam/})
    // .find({name: /.*mari/}); // .* name contain mariam
    .find({name: /.*Mohamm/i}) // i ignore capital or small
    //.count();

  console.log(students);
}
getStudent();
