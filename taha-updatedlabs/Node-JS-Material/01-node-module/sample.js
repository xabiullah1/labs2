function sum(a, b) {
  return a + b;
}

function sub(a, b) {
  return a - b;
}

// just allow other module to use sum function only
module.exports = sum;
// give aliase to function you can use alise
module.exports.theSumFunction = sum;
module.exports.theSubFunction = sub;
// we can use aliase with this way --professional way
module.exports = {
  theSumFunction: sum,
  theSubFunction: sub,
};
