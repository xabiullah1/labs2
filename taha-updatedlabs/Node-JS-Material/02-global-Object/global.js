// Node.js global objects are global in nature and available in all modules. You don't need to include these objects in your application; rather they can be used directly.
// These objects are modules, functions, strings and object etc
/* A list of Node.js global objects are given below:

__dirname
__filename
Console
Process
Buffer
setImmediate(callback[, arg][, ...])
setInterval(callback, delay[, arg][, ...])
setTimeout(callback, delay[, arg][, ...])
clearImmediate(immediateObject)
clearInterval(intervalObject)
clearTimeout(timeoutObject) 
*/
console.log(__dirname);
console.log(__filename);
console.log(process);
