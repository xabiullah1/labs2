/* The path.parse() method is used to return an object whose properties represent the given path. This method returns the following properties:

root (root name)
dir (directory name)
base (filename with extension)
ext (only extension)
name (only filename) */
const path = require("path");
const os = require("os");
const mypath = path.parse(__filename);

console.log(mypath);
// Printing os.freemem() value
console.log(os.freemem());
console.log(os.totalmem());

// Node.js program to demonstrate the
// os.freemem() method

// Allocating os module
