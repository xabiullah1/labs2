// const http = require("http");
// // please create server
// const server = http.createServer();
// // make server on
// server.on("connection", (socket) => {
//   console.log("new Connection is Comes");
// });
// //use this port as location same as classRoom number
// server.listen(3000);
// //
// console.log("Listining to port 3000");

// create server with request and response ///////
var http = require("http"); // Import Node.js core module

var server = http.createServer(function (req, res) {
  //create web server
  if (req.url === "/") {
    //check the URL of the current request

    // set response header
    res.writeHead(200, {"Content-Type": "text/html"});

    // set response content
    res.write(
      "<html><body><p>This is front end where we are going to connect to api  .</p></body></html>"
    );
    res.end();
  } else if (req.url === "/api/students") {
    res.writeHead(200, {"Content-Type": "application/json"});

    res.write(JSON.stringify({name: "taha", age: 20, salary: 1500}));
    res.end();
  } else if (req.url == "/admin") {
    res.writeHead(200, {"Content-Type": "text/html"});
    res.write("<html><body><p>This is admin Page.</p></body></html>");
    res.end();
  } else res.end("Invalid Request!");
});

server.listen(3000); //6 - listen for any incoming requests

console.log("Node.js web server at port 5000 is running..");
