const posts = [
  {title: "post 1", content: "Content of Post 1 "},
  {title: "post 1", content: "Content of Post 1 "},
];
const getPosts = () => {
  setTimeout(() => {
    let output = "";
    posts.forEach((post, indx) => {
      output += `<li>${post.title}</li>`;
    });
    document.body.innerHTML = output;
  }, 1000);
};
// will not create post cause it run after getpost to solve issue you have to use callback
const creatPost = (post) => {
  setTimeout(() => {
    posts.push(post);
  }, 2000);
};

getPosts();
creatPost({title: "post 3", content: "Content of Post 3 "});

// use call back ================
const posts = [
  {title: "post 1", content: "Content of Post 1 "},
  {title: "post 1", content: "Content of Post 1 "},
];
const getPosts = () => {
  setTimeout(() => {
    let output = "";
    posts.forEach((post, indx) => {
      output += `<li>${post.title}</li>`;
    });
    document.body.innerHTML = output;
  }, 1000);
};
// will not create post cause it run after getpost to solve issue you have to use callback
const creatPost = (post, callback) => {
  setTimeout(() => {
    posts.push(post);
    callback();
  }, 2000);
};

creatPost({title: "post 3", content: "Content of Post 3 "}, getPosts);
