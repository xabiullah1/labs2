// use call back ================
const posts = [
  {title: "post 1", content: "Content of Post 1 "},
  {title: "post 1", content: "Content of Post 1 "},
];
const getPosts = () => {
  setTimeout(() => {
    let output = "";
    posts.forEach((post, indx) => {
      output += `<li>${post.title}</li>`;
    });
    document.body.innerHTML = output;
  }, 1000);
};
// will not create post cause it run after getpost to solve issue you have to use callback
const creatPost = (post) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      posts.push(post);
      const done = false;
      if (done) {
        resolve();
      } else {
        reject("Error .......");
      }
    }, 2000);
  });
};
creatPost({title: "post 3", content: "Content of Post 3 "})
  .then(getPosts)
  .catch((err) => {
    console.log(err);
  });

// // promise all
const p1 = new Promise((resolve, reject) => {
  setTimeout(resolve, 2000, "hello");
});
// new shape of write promise
const p2 = Promise.resolve("HI");
const p3 = fetch("https://jsonplaceholder.typicode.com/users").then((res) =>
  res.json()
);

Promise.all([p1, p2, p3]).then((vals) => {
  console.log(vals);
});
