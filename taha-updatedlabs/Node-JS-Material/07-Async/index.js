// Async method
// console.log("First");
// setTimeout(() => {
//   console.log("second");
// });
// console.log("third");

// another prove will give undefine cause callback
console.log("First"); // p1
const student = getStudent(1); // p2 it will take time so will go to next process
console.log(student.fullName); // p3  it will print it with issue
console.log("Third"); //p4

function getStudent(studentId) {
  setTimeout(() => {
    console.log("Reading student from Database");
    return {id: studentId, fullName: "Taha saleh"};
  }, 2000);
}

// to solve this issue we are going to callbacks - promise - await / async
console.log("First"); // p1
getStudent(1, (student) => {
  console.log(student.fullName);
}); // p2 it will take time so will go to next process

console.log("Third"); //p4

function getStudent(studentId, callback) {
  setTimeout(() => {
    console.log("Reading student from Database");
    callback({id: studentId, fullName: "Taha saleh"});
  }, 2000);
}

//=======================================

////===================================
// another example issue will be happen may be getuser faster than add user
// var users = ["taha", "ali", "maryam"];
// function addUsers(userName) {
//   setTimeout(function () {
//     users.push(userName);
//   }, 200);
// }

// function getusers() {
//   setTimeout(() => {
//     console.log(users);
//   }, 100);
// }

// // first add user to database
// addUsers("ziad");
// // second get  updated users from database
// getusers();

// // solve issue using callback ======================
// var users = ["taha", "ali", "maryam"];
// function addUsers(userName, callback) {
//   setTimeout(function () {
//     users.push(userName);
//     callback();
//   }, 200);
// }

// function getusers() {
//   setTimeout(() => {
//     console.log(users);
//   }, 100);
// }

// // first add user to database
// addUsers("ziad", getusers);

// another example ========= CallBack hell
// Makes a burger
// makeBurger contains four steps:
//   1. Get beef
//   2. Cook the beef
//   3. Get buns for the burger
//   4. Put the cooked beef between the buns
//   5. Serve the burger (from the callback)
// We use callbacks here because each step is asynchronous.
//   We have to wait for the helper to complete the one step
//   before we can start the next step

const makeBurger = (nextStep) => {
  getBeef(function (beef) {
    cookBeef(beef, function (cookedBeef) {
      getBuns(function (buns) {
        putBeefBetweenBuns(buns, beef, function (burger) {
          nextStep(burger);
        });
      });
    });
  });
};

/// promise

// Pending: Initial State, before the Promise succeeds or fails
// Resolved: Completed Promise
// Rejected: Failed Promise
// const mypromise = new Promise((resolve, reject) => {
//   let conn = false;
//   if (conn) {
//     resolve("Connected");
//   } else {
//     reject("Fail To Connect");
//   }
// });

// mypromise
//   .then((message) => {
//     console.log(message);
//   })
//   .catch((message) => {
//     console.log(message);
//   });
