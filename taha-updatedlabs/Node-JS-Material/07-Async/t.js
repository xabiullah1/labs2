// callback hell ============AR
console.log("First");
getStudent(1, (student) => {
  getStudentCourses(student.fullName, (courses) => {
    console.log(courses);
  });
});
console.log("third");
function getStudent(studentId, callback) {
  setTimeout(() => {
    console.log("Reading student from Database");
    callback({id: studentId, fullName: "Taha saleh"});
  }, 2000);
}

function getStudentCourses(studentName, callback) {
  setTimeout(() => {
    console.log("student courses ");
    callback[("C#", "PYTHON", "HTML", "DevOps")];
  }, 2000);
}

// // using call back
// console.log("First");
// getStudent(1, (student) => {
//   getStudentCourses(student.fullName, (courses) => {
//     // callback function
//     console.log(courses);
//   });
// });
// console.log("third");
// function getStudent(studentId, callback) {
//   setTimeout(() => {
//     console.log("Reading student from Database");
//     callback({id: studentId, fullName: "Taha saleh"});
//   }, 1000);
// }
