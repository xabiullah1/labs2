const products = [
  {id: 1, name: "product 1", price: 50},
  {id: 2, name: "product 2", price: 50},
  {id: 3, name: "product 3", price: 50},
  {id: 4, name: "product4", price: 50},
  {id: 5, name: "product 5", price: 50},
  {id: 6, name: "product 6", price: 50},
  {id: 7, name: "product 7", price: 50},
  {id: 8, name: "product 8", price: 50},
  {id: 9, name: "product 9", price: 50},
  {id: 10, name: "product 10", price: 50},
  {id: 11, name: "product 11", price: 50},
];

function findInventory(element) {
  return element.name === "product4";
}

console.log(products.find(findInventory).id);
