const express = require("express");

const app = express();
app.use(express.json());

const products = [
  {id: 1, name: "product 1", price: 50},
  {id: 2, name: "product 2", price: 50},
  {id: 3, name: "product 3", price: 50},
  {id: 4, name: "product 4", price: 50},
  {id: 5, name: "product 5", price: 50},
  {id: 6, name: "product 6", price: 50},
  {id: 7, name: "product 7", price: 50},
  {id: 8, name: "product 8", price: 50},
  {id: 9, name: "product 9", price: 50},
  {id: 10, name: "product 10", price: 50},
  {id: 11, name: "product 11", price: 50},
  {id: 9, name: "product 9999", price: 50},
];

app.get("/api/products", (req, res) => {
  res.send(products);
});

// find product from list of products
app.delete("/api/products/:id/", (req, res) => {
  // search if product is found or not
  const product = products.find((p) => p.id === parseInt(req.params.id));
  // check if not found and send message
  if (!product) res.status(400).send("the product not found ");

  // call delete method against db
  const index = products.indexOf(product);
  // delete in this index and only remove 1
  products.splice(index, 1);
  res.send(products);
});

const port = process.env.PORT | 4000; // or port 5000
app.listen(port, () => {
  console.log(`listining in port ${port}`);
});
