// install loging for morgan ====== npm i morgan
// used between res and req
// https://expressjs.com/en/guide/using-middleware.html
const express = require("express");
const Joi = require("joi");
// thrd party for loging
var morgan = require("morgan");

const app = express();
// midlle ware to tell server you are going to receive JSON from res
app.use(express.json());
app.use(express.static("assets"));
// ?name=tahaddssd&price=3009

app.use(express.urlencoded());

//use morgan
app.use(morgan("tiny"));

const products = [
  {id: 1, name: "product 1", price: 50},
  {id: 2, name: "product 2", price: 50},
  {id: 3, name: "product 3", price: 50},
  {id: 4, name: "product 4", price: 50},
  {id: 5, name: "product 5", price: 50},
  {id: 6, name: "product 6", price: 50},
  {id: 7, name: "product 7", price: 50},
  {id: 8, name: "product 8", price: 50},
  {id: 9, name: "product 9", price: 50},
  {id: 10, name: "product 10", price: 50},
  {id: 11, name: "product 11", price: 50},
];
app.get("/", (req, res) => {
  res.send(products);
});

app.post("/api/products", (req, res) => {
  // using JOY
  // 1- Schema
  //   const schem = Joi.object({
  //     name:
  //     price: Joi.number().
  //   });
  const sc = Joi.object({
    id: Joi.number(),
    name: Joi.string().alphanum().min(3).max(40).required(),
    price: Joi.number().required(),
  });
  //Joi.string().alphanum().min(3).max(30).require(),
  // push an object to this array

  const product = {
    id: products.length + 1,
    name: req.body.name, // get name from body of request entered by user
    price: req.body.price,
  };

  // before any send you have to validate schema aginst object
  const {error} = sc.validate(product);
  if (error) res.status(400).send(error);

  products.push(products);
  res.send(product);
});

const port = process.env.PORT | 4000; // or port 5000
app.listen(port, () => {
  console.log(`listining in port ${port}`);
});
