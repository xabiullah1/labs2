const mongoose = require("mongoose");
// create schema allow us to define shape and content of database
var schema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  gender: String,
  status: String,
});

// userdb add any name for document -  schema is the shape of my document like name - email ...
const Userdb = mongoose.model("userdb", schema);

module.exports = Userdb;
